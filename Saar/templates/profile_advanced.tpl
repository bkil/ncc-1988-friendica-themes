<h1>{{$title}}</h1>
<table class="Profile">
  {{if $profile.fullname}}
  <tr>
    <th class="Bold">{{$profile.fullname.0}}</th>
    <td>{{$profile.fullname.1}}</td>
  </tr>
  {{/if}}
  {{if $profile.gender}}
  <tr>
    <th class="Bold">{{$profile.gender.0}}</th>
    <td>{{$profile.gender.1}}</td>
  </tr>
  {{/if}}
  {{if $profile.birthday}}
  <tr>
    <th class="Bold">{{$profile.birthday.0}}</th>
    <td>{{$profile.birthday.1}}</td>
  </tr>
  {{/if}}
  {{if $profile.age}}
  <tr>
    <th class="Bold">{{$profile.age.0}}</th>
    <td>{{$profile.age.1}}</td>
  </tr>
  {{/if}}
  {{if $profile.marital}}
  <tr>
    <th class="Bold">{{$profile.marital.0}}</th>
    <td>{{$profile.marital.1}}</td>
  </tr>
  {{/if}}
  {{if $profile.sexual}}
  <tr>
    <th class="Bold">{{$profile.sexual.0}}</th>
    <td>{{$profile.sexual.1}}</td>
  </tr>
  {{/if}}
  {{if $profile.pub_keywords}}
  <tr>
    <th class="Bold">{{$profile.pub_keywords.0}}</th>
    <td>{{$profile.pub_keywords.1}}</td>
  </tr>
  {{/if}}
  {{if $profile.homepage}}
  <tr>
    <th class="Bold">{{$profile.homepage.0}}</th>
    <td>{{$profile.homepage.1}}</td>
  </tr>
  {{/if}}
  {{if $profile.hometown}}
  <tr>
    <th class="Bold">{{$profile.hometown.0}}</th>
    <td>{{$profile.hometown.1}}</td>
  </tr>
  {{/if}}
  {{if $profile.politic}}
  <tr>
    <th class="Bold">{{$profile.politic.0}}</th>
    <td>{{$profile.politic.1}}</td>
  </tr>
  {{/if}}
  {{if $profile.religion}}
  <tr>
    <th class="Bold">{{$profile.religion.0}}</th>
    <td>{{$profile.religion.1}}</td>
  </tr>
  {{/if}}
  {{if $profile.about}}
  <tr>
    <th class="Bold">{{$profile.about.0}}</th>
    <td>{{$profile.interest.1}}</td>
  </tr>
  {{/if}}
  {{if $profile.likes}}
  <tr>
    <th class="Bold">{{$profile.likes.0}}</th>
    <td>{{$profile.likes.1}}</td>
  </tr>
  {{/if}}
  {{if $profile.dislikes}}
  <tr>
    <th class="Bold">{{$profile.dislikes.0}}</th>
    <td>{{$profile.dislikes.1}}</td>
  </tr>
  {{/if}}
  {{if $profile.contact}}
  <tr>
    <th class="Bold">{{$profile.contact.0}}</th>
    <td>{{$profile.contact.1}}</td>
  </tr>
  {{/if}}
  {{if $profile.music}}
  <tr>
    <th class="Bold">{{$profile.music.0}}</th>
    <td>{{$profile.music.1}}</td>
  </tr>
  {{/if}}
  {{if $profile.book}}
  <tr>
    <th class="Bold">{{$profile.book.0}}</th>
    <td>{{$profile.book.1}}</td>
  </tr>
  {{/if}}
  {{if $profile.tv}}
  <tr>
    <th class="Bold">{{$profile.tv.0}}</th>
    <td>{{$profile.tv.1}}</td>
  </tr>
  {{/if}}
  {{if $profile.film}}
  <tr>
    <th class="Bold">{{$profile.film.0}}</th>
    <td>{{$profile.film.1}}</td>
  </tr>
  {{/if}}
  {{if $profile.romance}}
  <tr>
    <th class="Bold">{{$profile.romance.0}}</th>
    <td>{{$profile.romance.1}}</td>
  </tr>
  {{/if}}
  {{if $profile.work}}
  <tr>
    <th class="Bold">{{$profile.work.0}}</th>
    <td>{{$profile.work.1}}</td>
  </tr>
  {{/if}}
  {{if $profile.education}}
  <tr>
    <th class="Bold">{{$profile.education.0}}</th>
    <td>{{$profile.education.1}}</td>
  </tr>
  {{/if}}
</table>