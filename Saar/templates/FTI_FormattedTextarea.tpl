<div>
  <div class="JS">
    <div class="Button RoundCorners Float" style="font-weight:bold;" onclick="javascript:void(FriendicaUI.FormattedText.InsertBBCode(this.parentNode.parentNode, 'b'));">B</div>
    <div class="Button RoundCorners Float" style="font-style:italic;" onclick="javascript:void(FriendicaUI.FormattedText.InsertBBCode(this.parentNode.parentNode, 'i'));">I</div>
    <div class="Button RoundCorners Float" style="text-decoration:underline;" onclick="javascript:void(FriendicaUI.FormattedText.InsertBBCode(this.parentNode.parentNode, 'u'));">U</div>
    <div class="Button RoundCorners Float" style="text-decoration:stroke;" onclick="javascript:void(FriendicaUI.FormattedText.InsertBBCode(this.parentNode.parentNode, 's'));">S</div>
    <div class="Button RoundCorners Float" onclick="javascript:void(FriendicaUI.FormattedText.InsertBBCode(this.parentNode.parentNode, 'quote'));"><q>&nbsp;</q></div>
    <div class="Button RoundCorners Float" onclick="javascript:void(FriendicaUI.FormattedText.InsertBBCode(this.parentNode.parentNode, 'size=xx-small'));">small</div>
    <div class="Button RoundCorners Float" onclick="javascript:void(FriendicaUI.FormattedText.InsertBBCode(this.parentNode.parentNode, 'size=xx-large'));">big</div>
    <div class="Button RoundCorners Float" onclick="javascript:void(FriendicaUI.FormattedText.InsertBBCode(this.parentNode.parentNode, 'code'));">code</div>
    <div class="Button RoundCorners Float" onclick="javascript:void(FriendicaUI.FormattedText.InsertURL(this.parentNode.parentNode));">URL</div>
    <div class="Button RoundCorners Float" onclick="javascript:void(FriendicaUI.FormattedText.InsertImageURL(this.parentNode.parentNode));">IMG</div>
    <a class="Right SmallText" href="help/BBCode">Formatting help</a>
  </div>
  <textarea name="{{$element.name|escape:'html'}}" class="FormattedTextarea Textarea Border RoundCorners"
    {{if $element.placeholder}}
        placeholder="{{$element.placeholder|escape:'html'}}"
    {{/if}}
    onkeyup="javascript:void(FriendicaUI.CountChars(this, this.parentNode.getElementsByClassName('CharCountField')[0]));"
    onfocus="javascript:void(FriendicaUI.ShowField(this.parentNode.getElementsByClassName('CharactersWrittenField')[0]));"
    >{{if $element.value}}{{$element.value|escape:'html'}}{{/if}}</textarea>
  <div class="JS Float">
    <span class="CharactersWrittenField" style="visibility:hidden;height:1.4em;margin-left:0.5em;margin-right:1em;float:left;">Characters written: <span class="CharCountField">0</span></span>
    <span class="Button Border RoundCorners" style="height:1.4em;float:left;" onclick="javascript:void(FriendicaUI.FormattedText.ShowPreview(this.parentNode.parentNode));">preview</span>
    
    <img class="Button RoundCorners SmallIcon SmallText" src="images/icons/image.png" alt="{{$upload|escape:'html'}}"
      onclick="javascript:void(FriendicaUI.Popup.Open(this.parentNode.parentNode, 'UploadImage'));"
      title="{{$upload|escape:'html'}}"></img>
    <img class="Button RoundCorners SmallIcon SmallText" src="images/icons/text.png" alt="{{$attach|escape:'html'}}"
      onclick="javascript:void(FriendicaUI.NewPost.UploadFile(this.parentNode.parentNode));"
      title="{{$attach|escape:'html'}}"></img>
    <img class="Button RoundCorners SmallIcon SmallText" src="images/icons/link.png" alt="{{$weblink}}"
      onclick="javascript:void(FriendicaUI.NewPost.InsertLink(this.parentNode.parentNode));"
      title="{{$weblink}}"></img>
    <img class="Button RoundCorners SmallIcon SmallText" src="images/icons/video.png" alt="{{$video|escape:'html'}}"
      onclick="javascript:void(FriendicaUI.NewPost.UploadVideo(this.parentNode.parentNode));"
      title="{{$video|escape:'html'}}"></img>
    <img class="Button RoundCorners SmallIcon SmallText" src="images/icons/audio.png" alt="{{$audio|escape:'html'}}"
      onclick="javascript:void(FriendicaUI.NewPost.UploadAudio(this.parentNode.parentNode));"
      title="{{$audio|escape:'html'}}"></img>
    <img class="Button RoundCorners SmallIcon SmallText" src="images/icons/gear.png" alt="{{$setloc|escape:'html'}}"
      onclick="javascript:void(FriendicaUI.NewPost.InsertLocation(this.parentNode.parentNode));"
      title="{{$setloc|escape:'html'}}"></img>
    <img class="Button RoundCorners SmallIcon SmallText" src="images/icons/gear.png" alt="{{$noloc|escape:'html'}}"
      onclick="javascript:void(FriendicaUI.NewPost.RemoveLocation(this.parentNode.parentNode));"
      title="{{$noloc|escape:'html'}}"></img>
  </div>
  <div class="PreviewField FieldRow FullWidth" style="display:none;"></div>
  {{if $element.extensionWidgets}}
  <div class="FieldRow">
    <div class="FormattedTextExtensions">
    {{foreach $element.extensionWidgets as $w}}
      {{include file="FTI_Widget.tpl" widget=$w}}
    {{/foreach}}
    </div>
  {{/if}}
</div>