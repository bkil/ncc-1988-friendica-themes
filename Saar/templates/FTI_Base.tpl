<?xml version="1.0" encoding="utf-8" ?>
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    {{foreach $page.headerItem as $header}}
      {{if $header.type == "js"}}
        <script type="text/javascript" src="{{$header.url}}"></script>
      {{elseif $header.type == "css"}}
        <link rel="stylesheet" href="{{$header.url}}"></link>
      {{/if}}
    {{/foreach}}
  </head>
  <body>
    {{if $page.popupItems}}
    <div class="PopupBackground">
      {{foreach $page.popupItems as $popup}}
        {{include file="FTI_PopupItem.tpl" item=$popup}}
      {{/foreach}}
    </div>
    {{/if}}
    <nav>
      {{foreach $page.navItem as $navItem}}
        {{include file="FTI_NavItem.tpl navItem=$navItem}}
      {{/foreach}}
    </nav>
    <aside>
      {{foreach $page.sideWidget as $sideWidget}}
        {{include file="FTI_Widget.tpl widget=$sideWidget}}
      {{/foreach}}
    </aside>
    <section>
      {{foreach $page.block as $pageBlock}}
        {{include file="FTI_PageBlock.tpl pageBlock=$pageBlock}}
      {{/foreach}}
    </section>
    {{if $page.footerItems}}
      {{foreach $page.footerItems as $pageFooterItem}}
        {{include file="FTI_PageFooterItem.tpl pageFooterItem=$pageFooterItem}}
      {{/foreach}}
    {{/if}}
  </body>
</html>
