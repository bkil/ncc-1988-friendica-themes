{{$blocks = []}}

{{$blocks[] = ["type" => "title", "title" => $ptitle, "layer" => 1, "description" => $nickname_block]}}

<!--<h1>{{$ptitle}}</h1>
<div class="Border RoundCorners Padding" style="margin-bottom:1em;">
{{$nickname_block}}
</div>-->

{{$formBlock = ["type" => "form", "title" => $h_pass, "layer" => 2]}}
{{$form = ["action" => "settings", "method" => "post", "autocomplete" => false, "submitValue" => $submit, "elements" => []]}}

{{$form.elements = [
  ["type" => "hidden", "name" => "form_security_token", "value" => $form_security_token],
  ["type" => "password", "name" => $password1.0, "value" => $password1.2, "required" => $password1.4, "autofocus" => $password1.5, "description" => $password1.3, "title" => $password1.1],
  ["type" => "password", "name" => $password2.0, "value" => $password2.2, "required" => $password2.4, "autofocus" => $password2.5, "description" => $password2.3, "title" => $password2.1],
  ["type" => "password", "name" => $password3.0, "value" => $password3.2, "required" => $password3.4, "autofocus" => $password3.5, "description" => $password3.3, "title" => $password3.1]  
  ]}}
{{if $oid_enable}}
  {{$form.elements[] = ["type" => "text", "name" => $openid.0, "value" => $openid.2, "title" => $openid.1, "description" => $openid.3]}}
{{/if}}
  
{{$formBlock.content = $form}}
{{$blocks[] = $formBlock}}


{{$formBlock = ["type" => "form", "title" => $h_basic, "layer" => 2]}}
{{$form = ["action" => "settings", "method" => "post", "autocomplete" => false, "submitValue" => $submit, "elements" => []]}}
{{$form.elements = [
  ["type" => "text", "name" => $username.0, "value" => $username.2, "required" => $username.4, "autofocus" => $username.5, "title" => $username.1, "description" => $username.3],
  ["type" => "email", "name" => $email.0, "value" => $email.2, "required" => $email.4, "autofocus" => $email.5, "title" => $email.1, "description" => $email.3],
  ["type" => "rawfield", "title" => $timezone.1, "description" => $timezone.3, "content" => $timezone.2],
  ["type" => "text", "name" => $defloc.0, "value" => $defloc.2, "required" => $defloc.4, "autofocus" => $defloc.5, "title" => $defloc.1, "description" => $defloc.3],
  ["type" => "checkbox", "name" => $allowloc.0, "value" => $allowloc.2, "required" => $allowloc.4, "autofocus" => $allowloc.5, "title" => $allowloc.1, "description" => $allowloc.3],
  ["type" => "password", "name" => $password4.0, "value" => $password4.2, "required" => $password4.4, "autofocus" => $password4.5, "description" => $password4.3, "title" => $password4.1]
]}}

{{$formBlock.content = $form}}
{{$blocks[] = $formBlock}}


{{$formBlock = ["type" => "form", "title" => $h_prv, "layer" => 2]}}
{{$form = ["action" => "settings", "method" => "post", "autocomplete" => false, "submitValue" => $submit, "elements" => []]}}

{{$form.elements = [
  ["type" => "hidden", "name" => "visibility", "value" => $visibility],
  ["type" => "text", "name" => $maxreq.0, "value" => $maxreq.2, "required" => $maxreq.4, "autofocus" => $maxreq.5, "title" => $maxreq.1, "description" => $maxreq.3]
]}}

{{$formBlock.content = $form}}
{{$blocks[] = $formBlock}}


{{foreach $blocks as $block}}
  {{include file="FTI_PageBlock.tpl" pageBlock=$block}}
{{/foreach}}

<form action="settings" method="post" autocomplete="off">
  
  <div class="Border RoundCorners Padding">
    <h3 class="Headline">{{$h_prv}}</h3>
    {{var_dump($profile_in_dir)}}
    {{$profile_in_net_dir}}
    {{$hide_friends}}
    {{$hide_wall}}
    {{$blockwall}}
    {{$blocktags}}
    {{$suggestme}}
    {{$unkmail}}
    {{include file="field_input.tpl" field=$cntunkmail}}
    {{include file="field_input.tpl" field=$expire.days}}
    <div class="Center">
      <input class="Button RoundCorners" type="submit" name="submit" value="{{$submit|escape:'html'}}" />
    </div>
  </div>
  
  <!-- TODO: a lot! -->
</form>