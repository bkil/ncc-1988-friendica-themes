<?php
/**
  * Name: Saar
  * Version: 0.1
  * Author: Moritz Strohm <https://mstrohm.sirius.uberspace.de/sozial/profile/moeeee>
  * Author: Moritz Strohm <https://mstrohm.sirius.uberspace.de/sozial/profile/moeeee>
  * Description: "Saar" is a theme written from scratch to build a compact, functional and beautiful theme. It includes rewritten JavaScript code without jQuery dependencies, CSS with responsive design and several color schemes.
*/

function saar_init(&$a)
{
  set_template_engine($a, 'smarty3');
  
  //additional HTML head elements are printed in here
  
}